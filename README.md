# Ansible Role Ubuntu Base

Provides a secure base installation for Ubuntu 20.04

| Variable                          | Default Value               | Explanation                                    |
|:----------------------------------|:----------------------------|:-----------------------------------------------|
| ubuntu_base_reboot_protect        | False                       | Keep the machine from rebooting after role     |
| ubuntu_base_update_protect        | False                       | Keep the machine from updating during role     |
| ubuntu_base_flavor                | default                     | "Flavor" of STIG controls to apply. See files/oscap_*_(fail|pass).yml  |
| ubuntu_base_aide_config           | files/aide.conf             | Path to AIDE configuration file to apply       |
| ubuntu_base_update_aide           | False                       | Force an aide --update                         |
| ubuntu_base_scan_only             | False                       | Only perform a SCAP scan, do not apply fixes   |
| ubuntu_base_scan_results_filename | "{{ ansible_hostname }}-xccdf_scan_results.xml" | Filename to save scan results to |
| ubuntu_base_enable_scan           | True                        | Run a SCAP scan before and after applying fixes |
| ubuntu_base_strip_insecure_bits   | True                        | Runs a script to strip insecure bits from all files |
| ubuntu_base_ignore_oscap_failures | False                       | Do not trigger a failure on OpenSCAP scan failures |
| ubuntu_base_scap_file             | U_CAN_Ubuntu_20-04_LTS_V1R1_STIG_SCAP_1-2_Benchmark.xml | STIG Benchmark file to use. This must be a BENCHMARK file |
| ubuntu_base_apparmor_profiles     | [{"name": "usr.sbin.sshd", "path": "/usr/share/apparmor/extra-profiles/usr/sbin/sshd"}] | A list of AppArmor extra profile definitions. See examples |
| ubuntu_base_enable_auto_update    | False                             | Enables the apt-daily-upgrade.timer systemd timer |
| ubuntu_base_syslog_ng_enable      | False                        | Configures syslog-ng as the logger for the system instead of rsyslog |
| ubuntu_base_syslog_ng_params      | None                         | A dictionary of parameters to pass to ansible_role_syslog. See `defaults/main.yml` for examples (commented out) |
| ubuntu_base_ntp_params            | See `defaults/main.yml`      | A dictionary of parameters to pass to ansible_role_ntp. See `defaults/main.yml` |



## Examples

```yaml
---
- name: Apply Ubuntu Base with extra AppArmor profiles
  hosts: all
  tasks:
    - name: Include Ubuntu Base role
      include_role:
        name: ansible_role_ubuntu_base
      vars:
        ubuntu_base_apparmor_profiles:
          - name: my-secure-service
            path: /usr/share/apparmor/extra-profiles/usr/sbin/my-secure-service
            content: "{{ vars_my_secure_service_apparmor_profile }}"
```
