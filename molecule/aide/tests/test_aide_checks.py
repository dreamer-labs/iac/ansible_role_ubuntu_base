import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_aide_detects_change(host):
    aide_check = host.run("aide --check")

    assert aide_check.rc == 4
