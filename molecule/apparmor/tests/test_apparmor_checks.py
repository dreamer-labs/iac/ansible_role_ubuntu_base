import os
import testinfra.utils.ansible_runner
import json

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_aa_profiles_enforcing(host):
    aa_profiles = json.loads(host.run("aa-status --json").stdout)

    assert aa_profiles["profiles"]["/usr/sbin/sshd"] == "enforce"

    assert aa_profiles["profiles"]["/usr/bin/my-cool-app"] == "enforce"
